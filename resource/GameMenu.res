"GameMenu"
{
	"1"
	{
		"label" "ResumeGame"
		"command" "ResumeGame"
		"OnlyInGame" "1"
	}
	"2"
	{
		"label" "Disconnect"
		"command" "Disconnect"
		"OnlyInGame" "1"
	}
	"3"
	{
		"label" "Reconnect"
		"command" "engine retry"
		"OnlyInGame" "1"
	}
	"4"
	{
		"label" "PlayerList"
		"command" "OpenPlayerListDialog"
		"OnlyInGame" "1"
	}
	"5"
	{
		"label" "_______________"
		"command" ""
		"OnlyInGame" "1"
	}
	"6"
	{
		"label" "Status"
		"command" "engine showconsole; status"
		"OnlyInGame" "1"
	}
	"7"
	{
		"label" "Take Screenshot"
		"command" "engine jpeg_quality 100; jpeg"
		"OnlyInGame" "1"
	}
	"8"
	{
		"label" "Admin"
		"command" "engine admin; sm_admin"
		"OnlyInGame" "1"
	}
	"9"
	{
		"label" "_______________"
		"command" ""
		"OnlyInGame" "1"
	}
	"10"
	{
		"label" "Start 1v1"
		"command" "engine say #.# start 1v1"
		"OnlyInGame" "1"
	}
	"11"
	{
		"label" "Start 2v2"
		"command" "engine say #.# start 2v2"
		"OnlyInGame" "1"
	}
	"12"
	{
		"label" "Start 3v3"
		"command" "engine say #.# start 3v3"
		"OnlyInGame" "1"
	}
	"13"
	{
		"label" "_______________"
		"command" ""
		"OnlyInGame" "1"		
	}
	"14"
	{
		"label" "DemoPlayer"
		"command" "engine demoui"
	}
	"15"
	{
		"label" ""
		"command" ""
	}
	"16"
	{
		"label" "CreateServer"
		"command" "OpenCreateMultiplayerGameDialog"
	}
	"17"
	{
		"label" "FindServers"
		"command" "OpenServerBrowser"
	}
	"18"
	{
		"label" "Options"
		"command" "OpenOptionsDialog"
	}
	"19"
	{
		"label" "_______________"
		"command" ""
		"OnlyInGame" "1"
	}
	"20"
	{
		"label" "Quit"
		"command" "engine quit"
	}
	"21"
	{
		"label" ""
		"command" ""
	}
	
}



